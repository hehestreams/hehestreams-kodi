import sys
import urllib
import urllib2
import urlparse
import json
import xbmcgui
import xbmcplugin
import xbmcaddon
import datetime
import calendar
import re

###
#
#   Hey person!
#   
#   You're probably reading this because you want to scrape HeheStreams content and give it 
#   to all your friends and e-friends. Well, go for it! But I don't think it'll work.
#
#   See, I control the keys needed to decrypt the streams. By controlling it, I can handle
#   authentication. I'm not going to send you the raw access links, because that's foolish.
#   
#   I don't do the authentication via sessions or cookies, instead I do it based on IP (among
#   other things). If the requesting IP (such as from your VLC player) doesn't match the IP
#   I have on file, along with the IP that's in that little `hmac` param (which is heavily 
#   encrypted and I wouldn't bother trying to decrypt it), I get a notification that something 
#   weird is going on.
#
#   If you're looking for inspiration though, here, have at it :) 
# 
###

base_url = sys.argv[0]
addon_handle = int(sys.argv[1])
my_addon = xbmcaddon.Addon()
api_key = my_addon.getSetting('api_key')
args = urlparse.parse_qs(sys.argv[2][1:]) # should probably map this since i don't use arrays but w/e
DATE = datetime.datetime.today().strftime("%Y-%m-%d")
api_endpoint = "http://hehestreams.xyz/api/v1"
xbmcplugin.setContent(addon_handle, 'movies')
DEBUG = True
CURRENT_SPORT = None 

# authenticate and get accesses
# this won't return MLB games because MLB is fucked up 
def get_sports():
    access = authenticate()
    return access  

# get games for a sport
# if NFL, allow for the season and week to be set
def get_games(date = ""): 
    season = "" # NFL only 
    week = "" # NFL only 

    if 'week' in args:
        week = args['week'][0]
    if 'season' in args:
        season = args['season'][0]    
   
    url = args['sport_name'][0].lower() + "/games?date=" + date + "&week=" + week + "&season=" + season 
    return api_request(url)

# get streams for a game
# sport/games/{uuid}/streams 
def get_streams(): 
    url = (args['sport_name'][0].lower() + "/games/" + args['uuid'][0] + "/streams")
    return api_request(url)

# simple enough, probably want to use a different iconimage though
def build_accesses(accesses):
    for access in accesses:
        url = build_url({'mode': 'sport', 'sport_name': access['name']})
        li = xbmcgui.ListItem(access['name'], iconImage='DefaultFolder.png')
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True)

    xbmcplugin.endOfDirectory(addon_handle)

# for whatever reason, this gets called twice if date is set from set_date
# instead of alerting a user, just display a different set_date_msg
def build_games(games):
    set_date_msg = "Set date"
    if len(games) == 0:
        set_date_msg = "No games for this date. Change it?"
        
    if args['sport_name'][0].lower() == 'nfl':
        url = build_url({'mode': 'set_season', 'sport_name': 'nfl'})
        li = xbmcgui.ListItem("Set season and week")
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True)

    if args['sport_name'][0].lower() == 'mls':
        url = build_url({'mode': 'set_date', 'sport_name': 'mls'})
        li = xbmcgui.ListItem(set_date_msg)
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True)
    
    if args['sport_name'][0].lower() == 'nba':
        url = build_url({'mode': 'set_date', 'sport_name': 'nba'})
        li = xbmcgui.ListItem(set_date_msg)
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True)

    for game in games:
        url = build_url({'mode': 'game', 'uuid': game['uuid'], 'sport_name': args['sport_name'][0]})
        li = xbmcgui.ListItem(game['title'], iconImage='DefaultFolder.png')
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True)

    xbmcplugin.endOfDirectory(addon_handle)

# again, probably could use different iconimages
# simple enough, except note isFolder=False
def build_streams(streams):
    for stream in streams:
        li = xbmcgui.ListItem(stream['source'], iconImage='DefaultFolder.png')
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=stream['url'], listitem=li, isFolder=False)
    xbmcplugin.endOfDirectory(addon_handle)

# could move into a different helper
def authenticate(): 
    try:
       req = urllib2.Request(api_endpoint + "/users/login")
       req.add_data("Kodi")
       req.add_header("ApiKey", api_key)
       response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        log('eeee')
        log(e.read())
        log('...eee')
        data = json.loads(e.read())
        if('error' in data):
            msg = data['error']
        else:
            msg = "There was an error."
        alert(msg)
        return False  
    else:
        #log(response)
        return json.load(response)

# generic
def api_request(url): 
    log(api_endpoint + "/" + url)
    try:
       req = urllib2.Request(api_endpoint + "/" + url)
       req.add_header("ApiKey", api_key)
       response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        data = json.loads(e.read())
        if('error' in data):
            msg = data['error']
        else:
            msg = "There was an error."
        alert(msg)
        return False  
    else:
        data = json.load(response)
        if('error' in data):
            alert(data['error'])
            return False
        return data     

# used for moving somewhere else        
def current_url(): 
    return sys.argv[2]

# standard
def build_url(query):
    return base_url + '?' + urllib.urlencode(query)

# what's log
def log(s):
    if DEBUG:
        print s
        xbmc.log(s)

# because the xbmcgui api is so fucking ugly
def alert(s): 
    xbmcgui.Dialog().ok("HeheStreams", s)

# not the prettiest, but it'll work
def set_season():
    url = build_url({'mode': 'sport', 'sport_name': 'nfl'})
    li = xbmcgui.ListItem("Go to this week's games")
    xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True)
    seasons = reversed(['2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016'])
    for s in seasons:
        url = build_url({'mode': 'set_week', 'sport_name': 'nfl', 'season': s})
        li = xbmcgui.ListItem(s + "-" + str(int(s) + 1) + " Season")
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True)
    xbmcplugin.endOfDirectory(addon_handle)

# again, not the prettiest, but it'll do
def set_week():
    weeks = {'Hall of Fame': 'pre_0'}

    for i in [1, 2, 3, 4]:
        string = 'Preseason Week ' + str(i)
        weeks[string] = 'pre_' + str(i)

    for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]:
        string = 'Regular Season Week ' + str(i)
        weeks[string] = 'reg_' + str(i)

    weeks['Wild Card Weekend'] = 'reg_18'
    weeks['Divisional Playoffs'] = 'reg_19'
    weeks['Conference Championships'] = 'reg_20'
    weeks['Pro Bowl'] = 'reg_21'
    weeks['Super Bowl'] = 'reg_22'

    for k, v in weeks.iteritems():
        url = build_url({'mode': 'sport', 'sport_name': 'nfl', 'season': args['season'][0], 'week': v})
        li = xbmcgui.ListItem(k)
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True)

    xbmcplugin.endOfDirectory(addon_handle)

# again-again, not the prettiest, but it will do.
# todo: show last entered date :|
def set_date():
    search_txt = ''
    dialog = xbmcgui.Dialog()
    game_day = dialog.input('Enter date (DD/MM/YYYY)', type=xbmcgui.INPUT_DATE)
    sport = args['sport_name'][0]

    #match = re.match('(\d{1,2})-(\d{1,2})-(\d{4})$', game_day)
    if game_day == '':
        uurl = 'plugin://plugin.hehestreams/?mode=sport&sport_name=' + sport + '&date='
        return xbmc.executebuiltin("Container.Refresh("+uurl+")")

    DATE = game_day.replace(' ', '')
    DATE = DATE.split("/")
    DATE = DATE[2] + '-' + DATE[1] + '-' + DATE[0]
    uurl = 'plugin://plugin.hehestreams/?mode=sport&sport_name=' + sport + '&date=' + DATE
    return xbmc.executebuiltin("Container.Refresh("+uurl+")")

    if match is not None:    
        DATE = game_day
        sport = args['sport_name'][0]
        uurl = 'plugin://plugin.hehestreams/?mode=sport&sport_name=' + sport + '&date=' + game_day
        xbmc.executebuiltin("Container.Refresh("+uurl+")")
    else:    
        if game_day != '':    
            msg = "The date entered is not in the format required."
            dialog = xbmcgui.Dialog() 
            ok = dialog.ok('Invalid Date', msg)
        sys.exit() # not sure why i'm doing this.

mode = args.get('mode', None)

if mode is None:
    accesses = get_sports()
    build_accesses(accesses)

elif mode[0] == 'sport':
    date = ""
    if 'date' in args:
        date = args['date'][0]

    games = get_games(date)
    build_games(games)

elif mode[0] == 'game':
    streams = get_streams()
    build_streams(streams)

elif mode[0] == 'set_date':
    set_date()

elif mode[0] == 'set_week':
    set_week()

elif mode[0] == 'set_season':
    set_season()        
    